#!/bin/bash

# Color definitions for messages
RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
PLAIN='\033[0m'

# Function to print red messages
red() {
    echo -e "${RED}$1${PLAIN}"
}

# Function to print green messages
green() {
    echo -e "${GREEN}$1${PLAIN}"
}

# Function to print yellow messages
yellow() {
    echo -e "${YELLOW}$1${PLAIN}"
}

# Function to determine CPU architecture
archAffix() {
    case "$(uname -m)" in
        i386 | i686) echo '386' ;;
        x86_64 | amd64) echo 'amd64' ;;
        armv8 | arm64 | aarch64) echo 'arm64' ;;
        s390x) echo 's390x' ;;
        *) red "Unsupported CPU architecture!" && exit 1 ;;
    esac
}

# Function to download and run the WARP optimization tool
runWarpTool() {
    local arch
    arch=$(archAffix)
	echo
    yellow "Downloading the WARP optimization tool..."
	echo
    wget -q "https://raw.githubusercontent.com/TheyCallMeSecond/WARP-Endpoint-IP/main/files/warp-linux-${arch}" -O warp
    if [ $? -ne 0 ]; then
        red "Failed to download the WARP tool."
        exit 1
    fi
	echo
    green "Download complete."
	echo

    yellow "Setting thread limits..."
    ulimit -n 102400
    if [ $? -ne 0 ]; then
        red "Failed to set thread limits."
        exit 1
    fi
    green "Thread limits set."
	echo

    yellow "Scanning the IPs. Please wait..."
    chmod +x warp
    if [ $? -ne 0 ]; then
        red "Failed to set executable permission for WARP tool."
        exit 1
    fi
    ./warp >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        red "Failed to execute the WARP tool."
        exit 1
    fi
    green "Scanning complete."
	echo
    filterResults
	echo
}

# Function to filter and display results
filterResults() {
    if [ -f result.csv ]; then
        yellow "Filtering results..."
        awk -F, '$3 != "timeout ms" && $2 != "100.00" {print}' result.csv | sort -t, -nk2 -nk3 | uniq > filtered_results.csv
        mv filtered_results.csv result.csv
        green "Filtered results saved to result.csv:"

        awk -F, '{print "Endpoint "$1" Packet loss rate "$2" Average delay "$3}' result.csv | head -21
    else
        red "Result file not found. Something went wrong."
        exit 1
    fi
}

# Function to fetch IP ranges from Cloudflare and generate IP list
generateIPList() {
	echo
    yellow "Fetching IP ranges from Cloudflare..."
	echo
    local ip_ranges
    ip_ranges=$(curl -s https://www.cloudflare.com/ips-v4)
    if [ -z "$ip_ranges" ]; then
        red "Failed to fetch IP ranges."
        exit 1
    fi
	echo
    yellow "Generating IP list from ranges..."
	echo
    local ranges=($ip_ranges)
    local temp=()

    for range in "${ranges[@]}"; do
        yellow "Processing range: $range"
        IFS=. read -r i1 i2 i3 i4 <<< "$(echo $range | cut -d '/' -f 1)"
        local mask
        mask=$(echo $range | cut -d '/' -f 2)
        local start=$(( (i1 << 24) + (i2 << 16) + (i3 << 8) + i4 ))
        local end=$(( start + (1 << (32 - mask)) - 1 ))

        for ((i = start; i <= end; i++)); do
            local ip
            ip="$(( (i >> 24) & 0xFF )).$(( (i >> 16) & 0xFF )).$(( (i >> 8) & 0xFF )).$(( i & 0xFF ))"
            temp+=($ip)
        done
    done

    echo "${temp[@]}" | sed -e 's/ /\n/g' | sort -u > ip.txt
    green "Generated IP list with ${#temp[@]} addresses."
	echo
}

# Main script execution
generateIPList
runWarpTool

echo
yellow "Cleaning up..."
rm -f warp ip.txt
green "Cleanup complete."
echo