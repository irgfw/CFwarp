#!/bin/bash

# Color definitions
RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
PLAIN='\033[0m'

# Function to print in red
red() {
    echo -e "${RED}\033[01m$1${PLAIN}"
}

# Function to print in green
green() {
    echo -e "${GREEN}\033[01m$1${PLAIN}"
}

# Function to print in yellow
yellow() {
    echo -e "${YELLOW}\033[01m$1${PLAIN}"
}

# Spinner function for visual feedback during long operations
spinner() {
    local pid=$!
    local delay=0.1
    local spinstr='|/-\'
    while ps a | awk '{print $1}' | grep -q $pid; do
        local temp=${spinstr#?}
        printf " [%c]  " "$spinstr"
        spinstr=$temp${spinstr%"$temp"}
        sleep $delay
        printf "\b\b\b\b\b\b"
    done
    printf "    \b\b\b\b"
}

# Function to determine the architecture suffix
archAffix() {
    case "$(uname -m)" in
        i386 | i686) echo '386' ;;
        x86_64 | amd64) echo 'amd64' ;;
        armv8 | arm64 | aarch64) echo 'arm64' ;;
        arm*) echo 'arm' ;;
        *) red "Unsupported CPU architecture!" && exit 1 ;;
    esac
}

# Function to download the WARP tool
download_tool() {
    local arch=$(archAffix)
    local base_url="https://gitlab.com/irgfw/CFwarp/-/raw/main/point"
    local url="${base_url}/${arch}?ref_type=heads&inline=false"

    wget "$url" -O warp >/dev/null 2>&1 &
    spinner
    chmod +x warp
}

# Function to clean up temporary files
cleanup() {
    rm -f warp ip.txt
}

# Function to run the WARP endpoint optimization tool
endpointyx() {
    ulimit -n 102400

    ./warp >/dev/null 2>&1 &
    spinner

    green "Top WARP Endpoint IP Addresses:"
    echo

    # Print the results sorted by latency
    awk -F, '$3!="timeout ms" {print}' result.csv | sort -t, -nk3 | uniq | head -11 | awk -F, '{printf "%-35s %-15s %-10s\n", $1, $2, $3}'

    echo
    green "Results are saved in result.csv file."
    echo
}

# Function to generate a list of IPv4 addresses
generate_ipv4_list() {
    local iplist=100
    local -a temp
    local prefixes=("162.159.192" "162.159.193" "162.159.195" "162.159.204" "188.114.96" "188.114.97" "188.114.98" "188.114.99")

    for prefix in "${prefixes[@]}"; do
        for ((i=0; i<iplist/8; i++)); do
            temp+=("${prefix}.$(($RANDOM % 256))")
        done
    done

    echo "${temp[@]}" | tr ' ' '\n' | sort -u | head -n $iplist > ip.txt &
    spinner
}

# Main function to coordinate the workflow
main() {
    clear
    echo
    green "WARP IPv4 Endpoint IP Scanner"
    echo
    sleep 1
    yellow "Downloading tool..."
    download_tool
    green "Done."
    echo
    sleep 1
    yellow "Generating IPv4 list..."
    generate_ipv4_list
    green "Done."
    echo
    sleep 1
    yellow "Scanning endpoints..."
    echo
    endpointyx
    echo
    cleanup
}

# Execute the main function
main
